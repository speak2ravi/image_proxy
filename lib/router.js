import readImagePage from '/lib/helpers/readImagePage';

if (Meteor.isClient) {
    Router.configure({
        notFoundTemplate: 'error',
        layoutTemplate: 'defaultLayout',
        noRoutesTemplate: 'noRoutesTemplate'
    });

    Router.map(
        function () {
            this.route('home', {
                path: '/',
                layoutTemplate: 'defaultLayout',
                action: function () {
                    this.render();
                }
            });
        }
    );
}

if(Meteor.isServer){
    Router.route('/getImage/:filename', function () {
        const name = this.params['filename'];
        const page = readImagePage(name);
        return this.response.end(page);
    }, {where: "server"});
}