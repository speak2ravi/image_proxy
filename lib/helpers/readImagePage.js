var fs = Npm.require('fs');

export default function readImagePage(name){
    const data = fs.readFileSync(Meteor.settings.imagesPath + name);
    const buffer = new Buffer(data, 'binary').toString('base64');
    const templateData ={ buffer, name };
    const imagePage = Spacebars.toHTML(templateData, Assets.getText('templates/imagePage.html'));
    return imagePage;
}