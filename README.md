# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Basic image server that reads images on servers local path and servers them to clients via route.


### How do I get set up? ###

* in settings.json, map the physical location of private/img/ to "imagesPath" variable
  example if my files were in c://temp/image_proxy/private/img/ then map that path to "imagesPath" in the settings file
* npm install to install the packages
* to run, execute the below from terminal/command prompt
  meteor --settings settings.json
